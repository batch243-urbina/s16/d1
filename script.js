console.log("Hello world!");

// Arithmetic Operations

let x = 1797;
console.log("The value of x is : " + x);

let y = 7831;
console.log("The value of y is : " + y);

// Addition
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multiplication
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division
let quotient = x / y;
console.log("Result of division operator: " + quotient);

// Modulo
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

let secondRemainder = x % y;
console.log("Result of modulo operator: " + secondRemainder);

// Assignment Operator
// Basic (=)
// Adds the value of the right operand and assigns the result to the value

let assignmentNumber = 8;
console.log(assignmentNumber);

// Addition assignment operator (+=)
// Adds the value of the right operand and assigns the result to the variable

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment operator (-=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

// Multiplication assignment operator (*=)
assignmentNumber *= 4;
console.log(
  "Result of multiplication assignment operator: " + assignmentNumber
);

// Division assignment operator (*=)
assignmentNumber /= 4;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators
let answer = 1 + 2 - (3 * 4) / 5;
console.log(answer);

let answer2 = 1 + 2 - (3 * 4) / (5 + 7);
console.log(answer2);

// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment and decrement was applied to

let z = 1;

// Pre-increment
let increment = ++z;
console.log("Result of z in pre-increment: " + z);
console.log("Result of increment in pre-increment: " + increment);

// Post-increment
increment = z++;
console.log("Result of z in post-increment: " + z);
console.log("Result of increment in post-increment: " + increment);
increment = z++;
console.log("Result of increment in post-increment: " + increment);

let p = 0;

// pre-decrement
let decrement = --p;
console.log("Result of p in pre-decrement: " + p);
console.log("Result of decrement in pre-decrement: " + decrement);

// post decrement
decrement = p--;
console.log("Result of p in post-decrement: " + p);
console.log("Result of decrement in post-decrement: " + decrement);
decrement = p--;
console.log("Result of decrement in post-decrement: " + decrement);

// TYPE COERCION
// automatic or implicit conversion of values from one data to another
// operations performed on different data types
// values automatically converted from one data type to another

let numA = "10";
let numB = 12;

// adding string and number
// ANSWER IS STRING

let sumAB = numA + numB;
console.log(sumAB);

// Not coercion
let numC = 16;
let numD = 14;

let sumCD = numC + numD;
console.log(sumCD);

// Boolean + number
let numE = true + 1;
console.log(numE);

// Comparison Operators
let juan = "juan";

// Equality Operator (==)
// checks whether equal or same value

let isEqual = 1 == 1;
console.log(typeof isEqual);

console.log(1 == 2);
console.log(1 == "1");
console.log(1 === "1");
console.log(juan == "juan");
console.log(juan === "juan");

// Inequality operator
// checks wether not equal or not the same value

let isNotEqual = 1 == 1;
console.log(typeof isNotEqual);

console.log(1 != 2);
console.log(1 != "1");
console.log(1 !== "1");
console.log(juan != "juan");
console.log(juan !== "juan");

// Relational Operators
// checks wether one value is greater of less than the other value

let a = 50;
let b = 65;

console.log(a > b);
console.log(a < b);

// GTE / LTE
console.log(a >= b);
console.log(a <= b);

let numStr = "30";
console.log(numStr > a);
console.log(numStr <= a);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// AND (&&)

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

let someRequirementsNotMet = !isLegalAge || isRegistered;
console.log(someRequirementsNotMet);
